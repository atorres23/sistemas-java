package Prueba2;

public class Profesor extends Persona {
	
	//private String nombre;
	//private String direccion;
	private String tlf;
	private String dni;
	private String asignatura;
	private String cargo;
	
	public Profesor(String nombre, String direccion, String tlf, String dni, String asignatura, String cargo) {
		super(nombre, direccion);
		//this.nombre = nombre;
		//this.direccion = direccion;
		this.tlf = tlf;
		this.dni = dni;
		this.asignatura = asignatura;
		this.cargo = cargo;
	}
	

	/*
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}*/
	public String getTlf() {
		return tlf;
	}
	public void setTlf(String tlf) {
		this.tlf = tlf;
	}
	public String getDNI() {
		if (validacion() == true) { //condicion si la funcion validacion() es verdadera entonces nos retorna el valor de dni
			return dni;
		}else {	//sino
			return "No es valido este DNI"; //nos retorna un mensaje que este DNI no es valido
		}
	}
	public void setDNI(String dNI) {
			dni = dNI;
	}
	public String getAsignatura() {
		return asignatura;
	}
	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	

	public boolean validacion() {		
		if(dni.length() != 9 && Character.isLetter(dni.charAt(8)) == false) { //condicion de si la longitud de la variable dni es diferente a 9 y que compruebe que el caracter 8 no es una letra 
			return false; //devolvera el valor de falso
		}
		if(cifras_caracter_DNI() == true) { //condicion de si cumple la funcion de cifras_caracter_DNI y es verdadero
			return true; //que devuelva el valor de verdadero
		}else { //sino
			return false; //devuelve valor de falso
		}
	}
	

	private boolean cifras_caracter_DNI() { //funcion booleana de verdadero o falso, para comprobar las cifras y el caracter de dni
		int introducidoDNI = Integer.parseInt(dni.substring(0,dni.length()-1)); //variable de tipo entero llamado introducidoDNI que su valor convierte la cadena de texto a un numero entero extrayendo los caracteres del 0 al 8, (que es la longitud del dni) sin incluirlo
		char introducidoLetra = dni.charAt(8); //variable de tipo caracter llamado introducidoLetra que sera igual al caracter de la posicion 8 (0-8,9 posiciones)
		char[] letraDNI = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'}; //array de caracteres, en funcion de la posicion de letra dependiendo del numero de dni que tengamos
		int resultado_letra; //variable entero llamado resultado_letra
		String numerosdni= dni.substring(0,dni.length()-1); //de dni te guarde caracteres del rango 0 a la longitud del dni de 9 que sera -1 para quitar la letra
		
		resultado_letra = introducidoDNI % 23;	//el valor de resultado_letra es igual al resto de la divison del valor introducidoDNI entre 23
		
		
		if(numerosdni.matches("[0-9]{8}") && introducidoLetra == letraDNI[resultado_letra]) { //condicion si numerosdni, definiendo su rango de 0-9 ({8} se puede repetir 8 veces) y si introducidoLetra es igual que letraDNI de valor el resultado_letra
			return true; //si se cumple devuelve verdadero
		}else{				 //sino
		return false;		//falso
	}
}	
	
	
	
	@Override
	public String toString() {
		return "Profesor ["+super.toString() + "Telefono = " + tlf + ", DNI = " + getDNI() + ", Asignatura = " + asignatura + ", Cargo = " + cargo + "]";
	}
 
	
	
	
}



		