package Prueba2;

public class Estudiante extends Persona {

	//private String nombre;	
	//private String direccion;
	private String NIA;	
	private String curso;
	private float nota_media;
	
	public Estudiante(String nombre, String direccion, String nIA, String curso, float nota_media) {
		super(nombre,direccion);
		//this.nombre = nombre;
		//this.direccion = direccion;
		this.NIA = nIA;		
		this.curso = curso;
		this.nota_media = nota_media;
	}
	
	
	
	
	/*
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}*/
			

	public String getNIA() {
		return NIA;
	}
	public void setNIA(String nIA) {
		NIA = nIA;
	}	
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public float getNota_media() {
		return nota_media;
	}
	public void setNota_media(float nuevoNota_media) {
		this.nota_media = nuevoNota_media;
	}
	
	@Override
	public String toString() {
		return "Estudiante ["+ super.toString() + "NIA = " + NIA +", Curso = " + curso + ", Nota media = " + nota_media + "]\n";
	}

	
	
}	