package Prueba2;

public class PersonalAdmin extends Persona {
	
	//private String nombre;
	//private String direccion;
	private String tlf;
	private String dni;
	private String puesto;
	
	
	/*
	public String getDNI() {
		if (validacion() == true) {
			return dni;
		}else {
			return "No es valido este DNI";
		}
	}
	public void setDNI(String dNI) {
			dni = dNI;
	}*/
	
	
	public PersonalAdmin(String nombre, String direccion, String tlf, String dni, String puesto) {
		super(nombre, direccion);
		this.tlf = tlf;
		this.dni = dni;
		this.puesto = puesto;
	}


	public String getTlf() {
		return tlf;
	}


	public void setTlf(String tlf) {
		this.tlf = tlf;
	}


	public String getDni() {
		if (validacion() == true) {
			return dni;
		}else {
			return "No es valido este DNI";
		}
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public String getPuesto() {
		return puesto;
	}


	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	

	public boolean validacion() {		
		if(dni.length() != 9 && Character.isLetter(dni.charAt(8)) == false) {
			return false;
		}
		if(cifras_caracter_DNI() == true) {
			return true;
		}else {
			return false;
		}
	}
	

	private boolean cifras_caracter_DNI() {
		int introducidoDNI = Integer.parseInt(dni.substring(0,dni.length()-1));
		char introducidoLetra = dni.charAt(8);
		char[] letraDNI = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
		int resultado_letra;
		String numerosdni= dni.substring(0,dni.length()-1);
		
		resultado_letra = introducidoDNI % 23;	
		
		if(numerosdni.matches("[0-9]{8}") && introducidoLetra == letraDNI[resultado_letra]) {
			return true;
		}else{				
		return false;		
	}
}


	@Override
	public String toString() {
		return "PersonalAdmin [" + super.toString() + "Telefono = " + tlf + ", DNI = " + getDni() + ", Puesto = " + puesto + "]";
	}	
	
	
 
	
	
	
}