package Prueba2;

import java.util.*;

import Colegio.Profesor;

public class Principal {
	
	public static void main (String[] args) {
		Scanner leerTeclado = new Scanner(System.in);
		
		System.out.println("Elige la categoria del usuario del que quiere introducir sus datos \n");
		System.out.println("Las opciones son:\n 1.Profesor \n 2.Estudiante \n 3.Personal de administracion \n 4.Salir");
		int opcion=leerTeclado.nextInt();
		boolean exit= false;	
		
		do {			
			switch (opcion) {
			case 1:
				Profesor(leerTeclado); 
				break;
			case 2:
				Estudiante(leerTeclado);
				break;
			case 3:
				PersonalAdmin(leerTeclado); 
				break;
			case 4:
				System.out.println("*Fin de formulario*"); 
				exit = true;
				break;
			default:
				System.out.println("Las opciones son:\n 1.Profesor \n 2.Estudiante \n 3.Personal de administracion \n 4.Salir");
		}
			if(!exit) {
				System.out.println("Si quieres a�adir otros dos usuarios pulse 1 para profesor, 2 para estudiante, 3 para personal de administracion o 4 para salir:\n");	
				opcion=leerTeclado.nextInt();
			}
			}while (exit==false);
			leerTeclado.close();
		}
		
		
		private static void Profesor(Scanner leerTeclado) {
			Profesor profesor = new Profesor(null, null, null, null, null, null);
			Scanner leerTecladoProfesor = new Scanner(System.in);
			
			try {
			System.out.print("Introduce los datos del primer profesor: \n");
			
			System.out.print("Nombre: ");
			String nuevoNombre = leerTecladoProfesor.nextLine();
			profesor.setNombre(nuevoNombre);
			
			System.out.print("Direccion: ");
			String nuevoDireccion = leerTecladoProfesor.nextLine();
			profesor.setDireccion(nuevoDireccion);
			
			System.out.print("Telefono: ");
			String nuevoTlf = leerTecladoProfesor.nextLine();
			profesor.setTlf(nuevoTlf);
			
			System.out.print("DNI: ");
			String nuevoDNI = leerTecladoProfesor.nextLine();
			profesor.setDNI(nuevoDNI);
			
			System.out.print("Asignatura: ");
			String nuevoAsignatura = leerTecladoProfesor.nextLine();
			profesor.setAsignatura(nuevoAsignatura);
			
			System.out.print("Cargo: ");
			String nuevoCargo = leerTecladoProfesor.nextLine();
			profesor.setCargo(nuevoCargo);
			
			
			Profesor uno = new Profesor(nuevoNombre, nuevoDireccion, nuevoTlf, nuevoDNI, nuevoAsignatura, nuevoCargo);
			
			
			System.out.print("Introduce los datos del segundo profesor: \n");
			leerTecladoProfesor.nextLine();
			
			System.out.print("Nombre: ");
			String nuevoNombre2 = leerTecladoProfesor.nextLine();
			profesor.setNombre(nuevoNombre2);
			
			System.out.print("Direccion: ");
			String nuevoDireccion2 = leerTecladoProfesor.nextLine();
			profesor.setDireccion(nuevoDireccion2);
			
			System.out.print("Telefono: ");
			String nuevoTlf2 = leerTecladoProfesor.nextLine();
			profesor.setTlf(nuevoTlf2);
			
			System.out.print("DNI: ");
			String nuevoDNI2 = leerTecladoProfesor.nextLine();
			profesor.setDNI(nuevoDNI2);
			
			System.out.print("Asignatura: ");
			String nuevoAsignatura2 = leerTecladoProfesor.nextLine();
			profesor.setAsignatura(nuevoAsignatura2);
			
			System.out.print("Cargo: ");
			String nuevoCargo2 = leerTecladoProfesor.nextLine();
			profesor.setCargo(nuevoCargo2);
			
			Profesor dos = new Profesor(nuevoNombre2, nuevoDireccion2, nuevoTlf2, nuevoDNI2, nuevoAsignatura2, nuevoCargo2);
			
			System.out.println("\n" + uno + "\n" + dos);	
			
			}catch(NoSuchElementException e){
				System.out.println("No es valido");
			}catch(StringIndexOutOfBoundsException e) {
				System.out.println("Datos introducidos no validos");
			}
		}
		
		private static void Estudiante(Scanner leerTeclado) {
			Estudiante estudiante = new Estudiante(null, null, null, null, 0);
			Scanner leerTecladoEstudiante = new Scanner(System.in);
			
			try {
			System.out.print("Introduce los datos del primer estudiante: \n");
			
			System.out.print("Nombre: ");
			String nuevoNombre = leerTecladoEstudiante.nextLine();
			estudiante.setNombre(nuevoNombre);
			
			System.out.print("Direccion: ");
			String nuevoDireccion = leerTecladoEstudiante.nextLine();
			estudiante.setDireccion(nuevoDireccion);
			
			System.out.print("NIA: ");
			String nuevoNIA = leerTecladoEstudiante.nextLine();
			estudiante.setNIA(nuevoNIA);
			
			System.out.print("Curso: ");
			String nuevoCurso = leerTecladoEstudiante.nextLine();
			estudiante.setCurso(nuevoCurso);
			
			System.out.print("Nota media: ");
			float nuevoNota_media = (float) leerTecladoEstudiante.nextDouble();
			estudiante.setNota_media(nuevoNota_media);	
			
			Estudiante a = new Estudiante(nuevoNombre, nuevoDireccion, nuevoNIA, nuevoCurso, nuevoNota_media);			
			
			
			System.out.print("Introduce los datos del segundo estudiante: \n");
			leerTecladoEstudiante.nextLine();
			System.out.print("Nombre: ");
			String nuevoNombre2 = leerTecladoEstudiante.nextLine();
			estudiante.setNombre(nuevoNombre2);
			
			System.out.print("Direccion: ");
			String nuevoDireccion2 = leerTecladoEstudiante.nextLine();
			estudiante.setDireccion(nuevoDireccion2);
			
			System.out.print("NIA: ");
			String nuevoNIA2 = leerTecladoEstudiante.nextLine();
			estudiante.setNIA(nuevoNIA2);
			
			System.out.print("Curso: ");
			String nuevoCurso2 = leerTecladoEstudiante.nextLine();
			estudiante.setCurso(nuevoCurso2);
			
			System.out.print("Nota media: ");
			float nuevoNota_media2 = (float) leerTecladoEstudiante.nextDouble();
			estudiante.setNota_media(nuevoNota_media2);	
			
			Estudiante b = new Estudiante(nuevoNombre2, nuevoDireccion2, nuevoNIA2, nuevoCurso2, nuevoNota_media2);
			
			System.out.println("\n" + a + "\n" + b + "\n");	
			}catch(InputMismatchException e){
				System.out.println("Tienes que introducir un valor numerico en nota media, se han introducido los demas datos");
			}			
		}
		
		private static void PersonalAdmin(Scanner leerTeclado) {
			PersonalAdmin personalAdmin = new PersonalAdmin(null, null, null, null, null);
			Scanner leerTecladoPersonalAdmin = new Scanner(System.in);
			
			try {
			System.out.print("Introduce los datos del primer personal de administracion: \n");
			
			System.out.print("Nombre: ");
			String nuevoNombre = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setNombre(nuevoNombre);
			
			System.out.print("Direccion: ");
			String nuevoDireccion = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setDireccion(nuevoDireccion);
			
			System.out.print("Telefono: ");
			String nuevoTlf = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setTlf(nuevoTlf);
			
			System.out.print("DNI: ");
			String nuevoDNI = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setDni(nuevoDNI);
			
			System.out.print("Puesto: ");
			String nuevoPuesto = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setPuesto(nuevoPuesto);
			
			PersonalAdmin pas1 = new PersonalAdmin(nuevoNombre, nuevoDireccion, nuevoTlf, nuevoDNI, nuevoPuesto);
			
			
			System.out.print("Introduce los datos del segundo personal de administracion: \n");
			leerTecladoPersonalAdmin.nextLine();
			
			System.out.print("Nombre: ");
			String nuevoNombre2 = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setNombre(nuevoNombre2);
			
			System.out.print("Direccion: ");
			String nuevoDireccion2 = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setDireccion(nuevoDireccion2);
			
			System.out.print("Telefono: ");
			String nuevoTlf2 = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setTlf(nuevoTlf2);
			
			System.out.print("DNI: ");
			String nuevoDNI2 = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setDni(nuevoDNI2);
			
			System.out.print("Puesto: ");
			String nuevoPuesto2 = leerTecladoPersonalAdmin.nextLine();
			personalAdmin.setPuesto(nuevoPuesto2);
			
			PersonalAdmin pas2 = new PersonalAdmin(nuevoNombre2, nuevoDireccion2, nuevoTlf2, nuevoDNI2, nuevoPuesto2);
			
			System.out.println("\n" + pas1 + "\n" + pas2 + "\n");
			}catch(NoSuchElementException e){
				System.out.println("No es valido");
			}catch(StringIndexOutOfBoundsException e) {
				System.out.println("Datos introducidos no validos");
			}
				
		}
}