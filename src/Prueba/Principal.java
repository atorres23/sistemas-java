package Prueba;

import java.util.Scanner;

import Colegio.Profesor;

public class Principal {
	
	public static void main (String[] args) {
		
		Scanner leerTeclado = new Scanner(System.in);
		System.out.println("Elige la categoria del usuario del que quiere introducir sus datos \n");
		System.out.println("Las opciones son:\n 1.Profesor \n 2.Estudiante \n 3.Personal de administracion \n 4.Salir");
		int opcion=leerTeclado.nextInt();
		boolean exit= false;	
		
		do {			
			switch (opcion) {
			case 1:
				Profesor(leerTeclado); 
				break;
			case 2:
				Estudiante(leerTeclado); 
				break;
			case 3:
				PersonalAdmin(leerTeclado); 
				break;
			case 4:
				System.out.println("*Fin de formulario*"); 
				exit = true;
				break;		
		}
			if(!exit) {
				System.out.println("�Desea rellenar otros datos de otro usuario? \n Las opciones son\n 1.Profesor \n 2.Estudiante \n 3.Personal de administracion \n 4.Salir");
				opcion=leerTeclado.nextInt();
			}
			}while (exit==false);
			leerTeclado.close();
		}
		
		
		private static void Profesor(Scanner leerTeclado) {
			Profesor profesor = new Profesor(null, null, null, null, null, null);
			Scanner leerTecladoProfesor = new Scanner(System.in);
			
			System.out.print("Introduce los datos del profesor: \n");
			
			System.out.print("Nombre: ");
			String nuevoNombre = leerTecladoProfesor.nextLine();
			profesor.setNombre(nuevoNombre);
			
			System.out.print("Direccion: ");
			String nuevoDireccion = leerTecladoProfesor.nextLine();
			profesor.setDireccion(nuevoDireccion);
			
			System.out.print("Telefono: ");
			String nuevoTlf = leerTecladoProfesor.nextLine();
			profesor.setTlf(nuevoTlf);
			
			System.out.print("DNI: ");
			String nuevodNI = leerTecladoProfesor.nextLine();
			profesor.setDNI(nuevodNI);
			
			System.out.print("Asignatura: ");
			String nuevoAsignatura = leerTecladoProfesor.nextLine();
			profesor.setAsignatura(nuevoAsignatura);
			
			System.out.print("Cargo: ");
			String nuevoCargo = leerTecladoProfesor.nextLine();
			profesor.setCargo(nuevoCargo);			
						
			System.out.print("Introduce los datos del segundo profesor: \n");
			
			System.out.print("Nombre: ");
			String nuevoNombre2 = leerTecladoProfesor.nextLine();
			profesor.setNombre(nuevoNombre2);
			
			System.out.print("Direccion: ");
			String nuevoDireccion2 = leerTecladoProfesor.nextLine();
			profesor.setDireccion(nuevoDireccion2);
			
			System.out.print("Telefono: ");
			String nuevoTlf2 = leerTecladoProfesor.nextLine();
			profesor.setTlf(nuevoTlf2);
			
			System.out.print("DNI: ");
			String nuevodNI2 = leerTecladoProfesor.nextLine();
			profesor.setDNI(nuevodNI2);
			
			System.out.print("Asignatura: ");
			String nuevoAsignatura2 = leerTecladoProfesor.nextLine();
			profesor.setAsignatura(nuevoAsignatura2);
			
			System.out.print("Cargo: ");
			String nuevoCargo2 = leerTecladoProfesor.nextLine();
			profesor.setCargo(nuevoCargo2);
			
			Profesor uno = new Profesor(nuevoNombre, nuevoDireccion, nuevoTlf, nuevodNI, nuevoAsignatura,nuevoCargo);
			Profesor dos = new Profesor(nuevoNombre2, nuevoDireccion2, nuevoTlf2, nuevodNI2, nuevoAsignatura2,nuevoCargo2);
						
			System.out.println("Profesores:\n\t" + uno + "," + dos);
			
		}
		
		private static void Estudiante(Scanner leerTeclado) {
			Estudiante estudiante = new Estudiante(null, null, null, null, 0);
			Scanner leerTecladoEstudiante = new Scanner(System.in);
			
			System.out.print("Introduce los datos del estudiante: \n");
			
			System.out.print("Nombre: ");
			String nuevoNombre = leerTecladoEstudiante.nextLine();
			estudiante.setNombre(nuevoNombre);
			
			System.out.print("Direccion: ");
			String nuevoDireccion = leerTecladoEstudiante.nextLine();
			estudiante.setDireccion(nuevoDireccion);
			
			System.out.print("NIA: ");
			String nuevoNIA = leerTecladoEstudiante.nextLine();
			estudiante.setNIA(nuevoNIA);
			
			System.out.print("Curso: ");
			String nuevoCurso = leerTecladoEstudiante.nextLine();
			estudiante.setCurso(nuevoCurso);
			
			System.out.print("Nota media: ");
			float nuevoNota_media = (float) leerTecladoEstudiante.nextDouble();
			estudiante.setNota_media(nuevoNota_media);
			
			Estudiante a = new Estudiante(nuevoNombre, nuevoDireccion, nuevoNIA, nuevoCurso, nuevoNota_media);
			System.out.println("Estudiante : \n\t" + a);		
		}
		
		private static void PersonalAdmin(Scanner leerTeclado) {
			PersonalAdmin pas1 = new PersonalAdmin("Antonio", "Mayor", "123456","21651465R","Administrador");
			PersonalAdmin pas2 = new PersonalAdmin("Alejandro", "Torres", "123456","4862234332","Conserje");
			System.out.println("Personal de admin:\n\t" + pas1+ "\n\t"+ pas2);			
		}
				
}