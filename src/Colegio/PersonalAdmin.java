package Colegio;

public class PersonalAdmin extends Persona {
	
	//private String nombre;
	//private String direccion;
	private String tlf;
	private String dni;
	private String puesto;
	
	public PersonalAdmin(String nombre, String direccion, String tlf, String dni, String asignatura) {
		super(nombre, direccion);
		//this.nombre = nombre;
		//this.direccion = direccion;
		this.tlf = tlf;
		this.dni = dni;
		this.puesto = asignatura;
	}
	
	
	/*
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}*/
	public String getTlf() {
		return tlf;
	}
	public void setTlf(String tlf) {
		this.tlf = tlf;
	}
	public String getDNI() {
		if (validacion() == true) {
			return dni;
		}else {
			return "No es valido este DNI";
		}
	}
	public void setDNI(String dNI) {
			dni = dNI;
	}
	public String getAsignatura() {
		return puesto;
	}
	public void setAsignatura(String asignatura) {
		this.puesto = asignatura;
	}	
	

	public boolean validacion() {		
		if(dni.length() != 9 && Character.isLetter(dni.charAt(8)) == false) {
			return false;
		}
		if(cifras_caracter_DNI() == true) {
			return true;
		}else {
			return false;
		}
	}
	

	private boolean cifras_caracter_DNI() {
		int introducidoDNI = Integer.parseInt(dni.substring(0,dni.length()-1));
		char introducidoLetra = dni.charAt(8);
		char[] letraDNI = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
		int resultado_letra;
		String numerosdni= dni.substring(0,dni.length()-1);
		
		resultado_letra = introducidoDNI % 23;	
		
		if(numerosdni.matches("[0-9]{8}") && introducidoLetra == letraDNI[resultado_letra]) {
			return true;
		}else{				
		return false;		
	}
}	
	
	
	
	@Override
	public String toString() {
		return "Profesor ["+super.toString() + "Telefono = " + tlf + ", DNI = " + getDNI() + ", Puesto = " + puesto + "]";
	}
 
	
	
	
}