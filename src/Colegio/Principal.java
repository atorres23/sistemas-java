package Colegio;

public class Principal {
	
	public static void main (String[] args) {
		
		Profesor uno = new Profesor("Antonio", "Mayor", "123456","47302347H","Ciencias","Tutor");
		Profesor dos = new Profesor("Susana", "Grande", "123456","12672762P","Matematicas","Jefa de departamento");
				
		Estudiante a = new Estudiante("Ana","007","Avenida piruleta","1�",(float) 8.5);
		Estudiante b = new Estudiante("Jose","001","Evergreem Terrace","2�",(float) 4.5);
	
		PersonalAdmin pas1 = new PersonalAdmin("Antonio", "Mayor", "123456","21651465R","Administrador");
		PersonalAdmin pas2 = new PersonalAdmin("Alejandro", "Torres", "123456","4862234332","Conserje");
		
		System.out.println("Profesores:\n\t" + uno.toString() +" \n\t" + dos.toString());
		System.out.println("Estudiantes:\n\t" + a.toString()+" \n\t" + b.toString());
		System.out.println("Personal de admin:\n\t" + pas1+ "\n\t"+ pas2);
		
	}
}