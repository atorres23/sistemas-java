package Sesi�n1;

import java.util.Scanner;

public class ejercicio_sergio {
	
	public static void main(String[] args) {
		int base = 5;
		String palabra = "Roma";
		Scanner leerTeclado=new Scanner(System.in);
		
		System.out.println("Bienvenido a juegos DAM");
		System.out.println("Las opciones son:\n 1->Dibujo figura \n 2->Jugar consonantes y vocales \n 3->Salir \n");
		int opcion=leerTeclado.nextInt();
		boolean exit= false;
		
		do {
			switch (opcion) {
			case 1:
				dibujar(base);
				break;
			case 2:
				jugarPalabra(palabra);
				break;
			case 3:
				System.out.println("*Gracias por jugar* \n Hasta la proxima");
				exit = true;
				break;
			default:
				System.out.println("Recuerde: las opciones son 1->Dibujar Piramide \n 2->Jugar Palabra(Roma) \n 3->Salir \n");
			}
			if(!exit) {
				System.out.println("�Desea seguir jugando? \n Las opciones son 1->Dibujar Piramide \n 2->Jugar Palabra(Roma) \n 3->Salir \n");
				opcion=leerTeclado.nextInt();
			}
		}while (exit==false);
		leerTeclado.close();
	}
	
	private static void dibujar(int b) {
		if (b % 2 == 1 && b > 0) {
			for (int i = 0; i < (b + 1) / 2;i++) {
				for (int j = 0; j < (b + 1) / 2 - i - 1; j++) {
					System.out.print("  ");
				}
				for (int j = 0; j < 2 * i + 1; j++) {
					System.out.print("* ");
				}
				System.out.println();
			}
		} else {
			System.out.println("La base de la piramide debe ser impar y mayor que 0");
	}
}	
	
	private static void jugarPalabra(String palabra) {
		
		for (int i = palabra.length() - 1;i >= 0; i--) {
			System.out.print(palabra.toUpperCase().charAt(i));
		}
		System.out.println();
	}
}