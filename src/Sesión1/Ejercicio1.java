package Sesi�n1;

public class Ejercicio1 {
		
	public static void main(String[] args) {
		String fragmentoNombreAlumnos = "Victor"; //el fragmentonombrealumnos tiene que ser un nombre que contenga la cadena "victor"
		String[] nombreAlumnos = {"Antonio", "Marta", "Victor Hugo", "David"}; //como vemos solo un nombre tiene la cadena victor
	
	int resultado =contarUsuarios(fragmentoNombreAlumnos, nombreAlumnos);//Cuenta el numero de alumnos que hay y el fragmento que quiere buscar en esos nombres.
	System.out.println("Total resultados: " + resultado); //Imprime por pantalla el resultado del total de los nombres que hay de la cadena fragmentonombrealumnos
	
	}
	
	
	static int contarUsuarios(String fragmentoNombreAlumno, String[] nombreAlumnos) { //Pone el entero con un array de nombrealumnos y la cadena fragmentonombrealumno.
		boolean encontrado = false;//Hace una asignacion y el valor de retorno es el nuevo valor.
		int totalEncontrados = 0; //El numero entero de los que se han encontrado en caso de ser falso.
		
		for (String nombreAlumnoActual : nombreAlumnos) { //Cadena el nombreAlumnoActual va a ser lo mismo que nombreAlumnos
			if (nombreAlumnoActual.contains(fragmentoNombreAlumno)) { //Si el nombrealumnoactual contiene fragmentonombrealumno y es verdadero saldra 1 por pantalla.
				encontrado = true;
			}
			
			if (encontrado) {//condicion si no encuentro el fragmentonombrealumnos en nombrealumnos para que siga mirando hasta encontrar "victor"
				totalEncontrados++;
				encontrado = false;
			}
		}
	return totalEncontrados;
}
} //Faltaba esta llave para ejecutar correctamente