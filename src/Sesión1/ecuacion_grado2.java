package Sesi�n1;

import java.util.Scanner;
import java.math.*;

public class ecuacion_grado2 {	
	
	public static void main(String[] args) {
		
		int	primero = 0, //numero x^2 
			segundo = 0, //numero x
			tercero = 0, //numero sin x
			parar = 0; //variable entero para poder parar
		
		double suma, //Primera operacion (suma) de la formula
			   resta; //Segunda operacion (resta) de la formula
		
		Scanner leerTeclado=new Scanner(System.in);
	
while(parar==0) {
	System.out.println("Introduce el valor del primer miembro con la incognita al cuadrado:");
	primero=leerTeclado.nextInt();
	System.out.println("Introduce el valor del segundo miembro con la incognita elevado a uno:");
	segundo=leerTeclado.nextInt();
	System.out.println("Introduce el valor del tercer miembro sin incognita:");
	tercero=leerTeclado.nextInt();
	System.out.println("La ecuacion es: " + primero +"(x^2)+" + segundo + "x+" + tercero + "= 0");
	System.out.println("�La ecuacion es correcta? Si es correcta, introduce cualquier numero diferente a 0 para continuar");
	parar=leerTeclado.nextInt();		
}
	leerTeclado.close();
	suma = (-segundo + Math.sqrt(segundo * segundo-4 * primero * tercero))/(2 * primero);
	resta = (-segundo - Math.sqrt(segundo * segundo-4 * primero * tercero))/(2 * primero);
	
	
	System.out.println("La primera solucion de la ecuacion es:" + suma);
	System.out.println("La segunda solucion de la ecuacion es:" + resta);
	System.out.println("Si la solucion es NaN quiere decir que no se puede resolver porque la raiz de la formula es negativa");	
	
}
}