package Sesi�n1;

import java.util.*;

public class juego_ejercicio {
	
	public static void main(String[] args) {
		Scanner leerTeclado=new Scanner(System.in);
		
		System.out.println("Bienvenido a los juegos de Sistemas del grado superior DAM");
		System.out.println("Las opciones son:\n 1.Dibujo figura \n 2.Jugar consonantes y vocales\n 3.Salir");
		int opcion=leerTeclado.nextInt();
		boolean exit= false;	
		
		do {			//dentro de este do habr� un switch con sus respectivos casos para elegir que queremos hacer
			switch (opcion) {
			case 1:
				dibujar(leerTeclado); //primer caso llamado dibujar
				break;
			case 2:
				jugarPalabra(leerTeclado); //segundo caso llamado jugarPalabra
				break;
			case 3:
				System.out.println("*Fin del juego*"); //tercer caso para salir
				exit = true;
				break;
			default:
				System.out.println("Recuerde: las opciones son\n 1.Dibujo figura \n 2.Jugar consonantes y vocales \n 3.Salir");
		}
			if(!exit) {
				System.out.println("�Desea seguir jugando? \n Las opciones son\n 1.Dibujo figura \n 2.Jugar consonantes y vocales \n 3.Salir");
				opcion=leerTeclado.nextInt();
			}
			}while (exit==false);
			leerTeclado.close();
		}
		
	private static void dibujar(Scanner leerTeclado){ //primera funcion llamada dibujar para cuando elijamos su caso
		System.out.print("Elige la figura que quieres dibujar:\n 1.Cuadrado\n 2.Triangulo rectangulo\n 3.Rombo\n");
		int eleccion;
		boolean exit = false;		
		do {			
			eleccion = leerTeclado.nextInt();
			switch (eleccion) {
			case 1: 
				cuadrado (leerTeclado);
				break;
			case 2:
				triangulo (leerTeclado);
				break;
			case 3:
				rombo (leerTeclado);
				break;
			default:
				System.out.println("Seleciona una opcion correcta");
		}
		}while (exit==false);
	}

	private static void cuadrado(Scanner leerTeclado) { //segunda funci�n llamada cuadrado para cuando elijamos el juego de dibujar y que dibuje el cuadrado
		int lado;
		System.out.print("Pon el lado o la base que quieres del cuadrado: ");
			lado = leerTeclado.nextInt();
			for	(int a = 0; a < lado; a++) { 	//algoritmo cuadrado vacio
				for (int b = 0; b < lado; b++)
					if (a == 0 || a == (lado - 1) || a == 0 || a == (lado - 1))
						System.out.print("*");
					else
						System.out.print(" ");
				System.out.println("");}}
		
	private static void triangulo(Scanner leerTeclado) { //tercera funci�n llamada triangulo para cuando elijamos el juego de dibujar y que dibuje el triangulo rectangulo
					int lado;
					System.out.println("Pon el lado o la base que quieres del triangulo rectangulo: ");
				lado = leerTeclado.nextInt();
				if (lado % 2 == 1 && lado > 0) { //algoritmo triangulo vacio
					for (int c = 0; c < lado; c++) {
						
						for (int d = 0; d <= c; d++)
							
							if (d == 0 || c == (lado - 1) || c == d)
								System.out.print("*");
							else
								System.out.print(" ");
						
						System.out.println();
					}
				} else {
					System.out.println("La base del triangulo rectangulo tiene que ser un numero impar para que pueda hacer el dibujo vacio.");
				}
	}
	
	private static void rombo(Scanner leerTeclado) { //cuarta funcion llamada rombo para cuando elijamos el dibujo el juego de dibujar y que dibuje el rombo
					int lado;
					System.out.println("Pon el lado o la base que quieres del rombo: ");
				lado = leerTeclado.nextInt();
				if (lado % 2 == 1 && lado > 0) { //algoritmo rombo relleno
					for(int y=1;y<=lado;y=y+2)
					{
			            for(int x=lado+1;x>=y;x=x-2)
			            {
			            	System.out.print(" ");
			            }
			            for(int z=0;z<y;z++)
			            {
			            	System.out.print("*");
			            }
			            System.out.println();
			        }
			        for(int y=lado;y>=1;y=y-2)
			        {
			        	for(int x=y;x<=lado+2;x=x+2)
			        	{
			        		System.out.print(" ");
			        	}       
			        	for(int z=y-2;z>0;z--)
			        	{
			        		System.out.print("*");
			        	}
			        	System.out.println();
			        }    
			    }else {
			    	System.out.println("La base del rombo debe ser un numero impar y mayor que 0.");
			    }
			}
	
	
	
	private static void jugarPalabra(Scanner LeerTeclado) {	
		String palabra;
		int i=0; //variable que sera parecido a un contador
		ArrayList<Character> vocales = new ArrayList<Character>(); //creacion de array de vocales para posteriormente ordenarlo
		ArrayList<Character> consonantes = new ArrayList<Character>();		
				 
		palabra = LeerTeclado.nextLine();	
		System.out.println("Pon la o las palabras que quieras:");
		palabra = LeerTeclado.nextLine();		
		System.out.println("Vocales:");
		 for (; i < palabra.length(); i++) {
			    char letra = palabra.charAt(i);

			    if (letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O'||letra == 'U' || letra == 'a'||letra == 'e' || letra == 'i'||letra == 'o' || letra == 'u')
			    	vocales.add(palabra.toLowerCase().charAt(i)); //a�ade el caracter a la celda, a minusculas y  devuelve el caracter del valor de i del for
		 }
		 		Collections.sort(vocales);		 //sirve para ordenar un array
		 		System.out.println(vocales);
		 		
		 if (i==palabra.length()) {
			 System.out.println("Consonantes:");
		 for (int j = 0; j < palabra.length(); j++) {
			    char letra = palabra.charAt(j);

			    if (letra != ' ' && letra != 'A' && letra != 'E' && letra != 'I' && letra != 'O'&& letra != 'U' && letra != 'a'&& letra != 'e' && letra != 'i'&& letra != 'o' && letra != 'u')
			    	consonantes.add(palabra.toLowerCase().charAt(j));
		 }
		    Collections.sort(consonantes);		 
	 		System.out.println(consonantes);	 		
		 }
	}
}